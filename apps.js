// 子应用配置

let ipAddr = ''
if (process.env.NODE_ENV === 'production') {
  // 生产环境
  ipAddr = '//101.33.205.30:'
}else {
  // 开发环境
  ipAddr = '//localhost:'
}

const genActiveRule = (hash) => (location) => location.hash.startsWith(hash)

//子应用列表
let apps = [
  {
    name:'app1',
    entry: ipAddr + '8084', 
    container:'#qk-container',//子应用的容器节点的选择器（vue一般为app）
    activeRule: genActiveRule('#/app1'),//访问子应用的规则，比如：主应用为localhost:8081，那访问该子应用的url应为localhost:8081/subapp
  },
  {
    name:'app2',
    entry: ipAddr + '8082',//子应用的地址，这里演示是本地启动的地址。
    container:'#qk-container',//子应用的容器节点的选择器（vue一般为app）
    activeRule: genActiveRule('#/app2'),//访问子应用的规则，比如：主应用为localhost:8081，那访问该子应用的url应为localhost:8081/subapp
  },
]

export default apps;