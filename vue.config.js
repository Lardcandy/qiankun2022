const packageName = require('./package').name

module.exports = {
    devServer: {
        headers: {
            'Access-Control-Allow-Origin': '*',
        },
        proxy: {
            //axios跨域改造
          //  '/360bg': {
          //    target:'https://www.yuanxiapi.cn/api/360bg', // 你请求的第三方接口
          //    changeOrigin:true, // 在本地会创建一个虚拟服务端，然后发送请求的数据，并同时接收请求的数据，这样服务端和服务端进行数据的交互就不会有跨域问题
          //    pathRewrite:{  // 路径重写，
          //      '^/360bg': ''  // 替换target中的请求地址，也就是说/api=/target，请求target这个地址的时候直接写成/api即可。
          //    }
          //  },
           '/yyNewsApi': {
            target:'https://news.api.bdymkt.com/news', // 你请求的第三方接口
            secrue: true,
            changeOrigin: true, // 在本地会创建一个虚拟服务端，然后发送请求的数据，并同时接收请求的数据，这样服务端和服务端进行数据的交互就不会有跨域问题
            pathRewrite:{  // 路径重写，
              '^/yyNewsApi': ''  // 替换target中的请求地址，也就是说/api=/target，请求target这个地址的时候直接写成/api即可。
            }
          }
         },
    },
    configureWebpack: {
        output: {
            library: `${packageName}-[name]`,
            libraryTarget: 'umd',
            jsonpFunction: `webpackJsonp_${packageName}`,

        }
    }
}