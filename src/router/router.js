// 引入vue-router对象
import { 
    createRouter, 
    createWebHashHistory 
    // createWebHistory 
} from "vue-router";
import dashboard from "../components/dashboard/dashboard.vue"
import jsRedBook from "../components/jsRedBook/jsRedBook.vue"
import score from "../components/tools/score.vue"

// 定义路由数组
const routes = [
    {
        path: '/',
        component: dashboard,
        children: [
            {
                path: '/dashboard',
                component: dashboard
            }
        ],
        
    },
    {
        path: '/jsRedBook',
        component: jsRedBook
    },
    {
        path: '/tools/score',
        component: score
    }
]

// 创建路由
const router = createRouter({
    // hash模式：createWebHashHistory，history模式：createWebHistory
    history: createWebHashHistory(),
    routes,
});

// 输出对象
 export default router;