import { createApp } from 'vue'
import less from 'less'
import Antd from 'ant-design-vue';
import 'ant-design-vue/dist/antd.css';
import { registerMicroApps, start } from 'qiankun';  //qiankun配置
import axios from 'axios'
import VueAxios from 'vue-axios'
import apps from '../apps'
import router from './router/router'
import App from './App.vue'
// import cors from 'cors'

//注册子应用
registerMicroApps(apps);

//启动qiankun
start();

// vue3
createApp(App).use(router).use(VueAxios, axios).use(less).use(Antd).mount('#app')
